# Geolocal

Desafio feito em Node para integração com a Api da Google Geocoding para cálculo de distância euclidiana entre N pontos.

### Prerequisities

Para executar este projeto deverá ter o Node instalado.

* [Node](https://nodejs.org/en/download/)

Para utilização do ESLint deverá seguir as instruções.

* [ESLint](https://eslint.org/docs/user-guide/getting-started/)

Para utilização do Prettier deverá seguir as instruções.

* [Prettier](https://prettier.io/docs/en/install.html)

Para utilização do Nodemon deverá seguir as instruções.

* [Nodemon](https://www.npmjs.com/package/nodemon)

Para utilização do Yarn deverá seguir as instruções.

* [Yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable)

### Usage

#### Parameters

Lista dos parametros do sistema.

##### Instalar depêndencias

```shell
yarn install
```

##### Iniciar a aplicação em modo de Desenvolvedor

```shell
yarn dev
```

#### Environment Variables

* `NODE_ENV` - Ambiente da aplicação.
* `PORT` - Porta da aplicação.
* `GOOGLE_GEOCODING_URI` - Link da Api de Geocoding da Google.
* `GOOGLE_API_KEY` - Chave de Api gerada pela Google.

#### Useful File Locations

* `/src/api/controllers` - Funções da controllers do express route
* `/src/api/services` - Regras de negócio
* `/src/api/subscribers` - Eventos async
* `/config` - Configuração das variaveis de ambiente
* `/utils` - Trechos de código que podem ser utilizados diversas vezes
* `/routes` - Definição de rotas express

## Built With

* node v15.12.0

## Find Me

* [GitLab](https://gitlab.com/Toborn/)

## Docs
* [Postman](https://documenter.getpostman.com/view/8679946/TzCJgVXo)

## Versioning

Utilizamos [SemVer](http://semver.org/) para versionamento.

## Authors

* **Marcio Braga** - *Initial work* - [Toborneiro](https://gitlab.com/Toborn)


## Acknowledgments

* Padrões baseados de [Github](https://github.com/armoucar/javascript-style-guide)
