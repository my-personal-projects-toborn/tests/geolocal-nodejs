import App from './app';
import config from './config';

App.listen(config.api.port);
