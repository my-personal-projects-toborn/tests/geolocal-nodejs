import axios from 'axios';
import config from '../../config';

export default async function geoCoding(address) {
  return axios({
    method: 'get',
    url: `${config.googleApi.uri}address=${encodeURIComponent(address)}&key=${
      config.googleApi.apiKey
    }`,
  });
}
