import joi from 'joi';

export default function getDistanceValidator(payload) {
  const schema = joi.object({
    address: joi.array().items(joi.string()).required(),
  });
  return schema.validateAsync(payload);
}
