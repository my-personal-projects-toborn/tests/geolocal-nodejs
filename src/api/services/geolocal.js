import calculateDistance from '../../utils/euclideanDistance';
import geoCoding from '../subscribers/googleMaps';

function getAddressDistance(address1, address2) {
  const result = calculateDistance(
    address1.lat,
    address1.lon,
    address2.lat,
    address2.lon
  );
  const distance = {
    firstAddress: address1.name,
    secondAddress: address2.name,
    kilometers: result.toFixed(2),
  };
  return { distance };
}

function getNearest(distances) {
  const nearest = distances[0].distance;
  return nearest;
}

function getFarthest(distances) {
  const farthest = [...distances].pop().distance;
  return farthest;
}

function sortDistances(distances) {
  return distances.sort(
    (a, b) => a.distance.kilometers - b.distance.kilometers
  );
}

function getAddressCombination(addresses) {
  const results = [];
  for (let i = 0; i < addresses.length - 1; i += 1) {
    for (let j = i + 1; j < addresses.length; j += 1) {
      results.push(getAddressDistance(addresses[i], addresses[j]));
    }
  }
  return results;
}

function getAddressCoordinate(result) {
  return {
    name: result.formatted_address,
    lat: result.geometry.location.lat,
    lon: result.geometry.location.lng,
  };
}

async function getAddress(addresses) {
  return Promise.all(
    addresses.map(async (address) => {
      const result = await geoCoding(address);
      return getAddressCoordinate(result.data.results[0]);
    })
  );
}

export default async function getDistance({ address }) {
  const adresses = await getAddress(address);
  const result = getAddressCombination(adresses);
  const distances = sortDistances(result);
  const nearest = getNearest(distances);
  const farthest = getFarthest(distances);
  return { distances, nearest, farthest };
}
