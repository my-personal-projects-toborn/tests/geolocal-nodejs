import serviceGeolocal from '../services/geolocal';
import validatorGeolocal from '../validators/geolocal';

exports.getDistance = async (req, res) => {
  try {
    await validatorGeolocal(req.query);
    const result = await serviceGeolocal(req.query);
    res.status(200).send({ error: false, payload: result });
  } catch (error) {
    res
      .status(400)
      .send({ error: true, message: error.message, detail: error });
  }
};
