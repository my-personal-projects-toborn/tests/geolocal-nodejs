import { Router } from 'express';
import controller from '../api/controllers';

const route = Router();

module.exports = (app) => {
  app.use('/geolocal', route);
  route.get('/', controller.geolocal.getDistance);
};
