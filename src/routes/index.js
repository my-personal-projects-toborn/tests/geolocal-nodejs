import { Router } from 'express';
import routeGeolocal from './geolocal';

const router = new Router();

router.get('/status', (req, res) =>
  res.status(200).json({ message: 'Online' })
);

routeGeolocal(router);

router.use((req, res) => {
  res.status(404).send({
    error: true,
    message: 'Not Found',
    detail: 'URI Not Found. Try /geolocal',
  });
});

export default router;
