import dotenv from 'dotenv';

dotenv.config({
  path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
});

export default {
  api: {
    env: process.env.NODE_ENV,
    port: process.env.PORT,
  },
  googleApi: {
    uri: process.env.GOOGLE_GEOCODING_URI,
    apiKey: process.env.GOOGLE_API_KEY,
  },
};
